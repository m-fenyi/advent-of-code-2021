input_filename = "3 INPUT.txt"


with open(input_filename) as input_file:
    data = [line.rstrip() for line in input_file]

bit_len = len(data[0])

frequency = [0 for i in range(bit_len)]
for line in data:
    for j, char in enumerate(line):
        frequency[j] += int(char)

gamma_str = ""
for f in frequency:
    if f > len(data)/2:
        gamma_str += "1"
    else:
        gamma_str += "0"

gamma_rate = int(gamma_str,2)
epsilon_rate = 2**len(frequency)-1-gamma_rate

print(gamma_rate)
print(epsilon_rate)
print(gamma_rate*epsilon_rate)

most_common_list = data
least_common_list = data
for i in range(bit_len):
    frequency_1 = 0
    if len(most_common_list) != 1:
        for rating in most_common_list:
            if rating[i] == "1":
                frequency_1 += 1
        if frequency_1 >= len(most_common_list)/2:
            most_common_list = [r for r in most_common_list if r[i] == "1"]
        else:
            most_common_list = [r for r in most_common_list if r[i] == "0"]
        print(len(most_common_list), frequency_1)
        
    frequency_1 = 0
    if len(least_common_list) != 1:
        for rating in least_common_list:
            if rating[i] == "1":
                frequency_1 += 1
        if frequency_1 < len(least_common_list)/2:
            least_common_list = [r for r in least_common_list if r[i] == "1"]
        else:
            least_common_list = [r for r in least_common_list if r[i] == "0"]
        print(len(least_common_list), frequency_1)

print (most_common_list, int(most_common_list[0],2))
print (least_common_list, int(least_common_list[0],2))
print(int(most_common_list[0],2)* int(least_common_list[0],2))

