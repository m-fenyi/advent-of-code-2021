input_filename = "1 INPUT.txt"


increase_count = 0
with open(input_filename) as input_file:
    prev_value = int(input_file.readline())
    for line in input_file:
        if int(line) > prev_value:
            increase_count += 1
        prev_value = int(line)

print(increase_count)

increase_count = 0
value_queue = []
queue_length = 3
with open(input_filename) as input_file:
    for line in input_file:
        if len(value_queue) < queue_length:
            value_queue.append(int(line))
        else:
            prev_sum = sum(value_queue)
            value_queue.append(int(line))
            value_queue.pop(0)
            if sum(value_queue) > prev_sum:
                increase_count+=1


print(increase_count)