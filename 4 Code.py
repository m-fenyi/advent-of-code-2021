input_filename = "4 INPUT.txt"

grid_size = 5
bingo_numbers = []
bingo_grids = []
bingo_grid = []

with open(input_filename) as input_file:
    bingo_numbers = [int(value) for value in input_file.readline().split(",")]
    for line in input_file:
        bingo_line = line.rstrip().split(" ")
        if len(bingo_line) <= 1 and len(bingo_grid) > 0:
            bingo_grids.append(bingo_grid)
            bingo_grid = []
        elif len(bingo_line) > 1:
            bingo_grid.append([int(v) for v in bingo_line if len(v) > 0])

def check_grid(used_grid):
    for line in used_grid:
        if all(v<0 for v in line):
            return True
    
    for i in range(len(used_grid)):
        column = True
        for j in range(len(used_grid)):
            if used_grid[j][i] >= 0:
                column = False
                break
        if column:
            return True

    diag_1 = True
    diag_2 = True
    for i in range(len(used_grid)):
        if used_grid[i][i] >= 0:
            diag_1 = False
        if used_grid[i][len(used_grid)-1] >= 0:
            diag_2 = False

    return diag_1 or diag_2


for bingo_number in bingo_numbers:
    winnings =[]
    for grid_number in range(len(bingo_grids)):
        for line_number in range(len(bingo_grids[grid_number])):
            for col_number in range(len(bingo_grids[grid_number][line_number])):
                if bingo_grids[grid_number][line_number][col_number] == bingo_number:
                    bingo_grids[grid_number][line_number][col_number] = bingo_number*-1 -1
        if check_grid(bingo_grids[grid_number]):
            grid_value = 0
            for line in bingo_grids[grid_number]:
                for number in line:
                    if number >= 0:
                        grid_value+= number
            print(f"Winning Grid {grid_number}, {grid_value}*{bingo_number} = {bingo_number*grid_value}")
            winnings.append(grid_number)
    for i, win in enumerate(winnings):
        bingo_grids.pop(win-i)