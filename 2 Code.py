input_filename = "2 INPUT.txt"

horizontal = 0
vertical = 0
with open(input_filename) as input_file:
    for line in input_file:
        commands = line.split(' ')
        if commands[0] == "forward":
            horizontal += int(commands[1])
        elif commands[0] == "down":
            vertical += int(commands[1])
        elif commands[0] == "up":
            vertical -= int(commands[1])

print(f"Horizontal: {horizontal}")
print(f"Vertical  : {vertical}")
print(f"Position  : {horizontal * vertical}")

horizontal = 0
vertical = 0
aim = 0
with open(input_filename) as input_file:
    for line in input_file:
        commands = line.split(' ')
        if commands[0] == "forward":
            horizontal += int(commands[1])
            vertical += aim * int(commands[1])
        elif commands[0] == "down":
            aim += int(commands[1])
        elif commands[0] == "up":
            aim -= int(commands[1])

print(f"Horizontal: {horizontal}")
print(f"Vertical  : {vertical}")
print(f"Position  : {horizontal * vertical}")